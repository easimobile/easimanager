package com.easipos.em.repositories.precheck

import com.easipos.em.api.requests.precheck.CheckVersionRequestModel
import com.easipos.em.datasource.DataFactory
import io.reactivex.Single

class PrecheckDataRepository(private val dataFactory: DataFactory) : PrecheckRepository {

    override fun checkVersion(model: CheckVersionRequestModel): Single<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}
