package com.easipos.em.repositories.notification

import com.easipos.em.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.em.api.requests.notification.RemoveFcmTokenRequestModel
import io.reactivex.Completable

interface NotificationRepository {

    fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable

    fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable
}
