package com.easipos.em.repositories.notification

import com.easipos.em.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.em.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.em.datasource.DataFactory
import io.reactivex.Completable

class NotificationDataRepository(private val dataFactory: DataFactory) : NotificationRepository {

    override fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable =
        dataFactory.createNotificationDataSource()
            .registerFcmToken(model)

    override fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable =
        dataFactory.createNotificationDataSource()
            .removeFcmToken(model)
}