package com.easipos.em.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AlertDialog
import com.easipos.em.R
import kotlinx.android.synthetic.main.alert_dialog_logout.*

class LogoutAlertDialog(context: Context,
                        private val onLogout: () -> Unit) : AlertDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_logout)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        button_no.setOnClickListener {
            dismiss()
        }

        button_yes.setOnClickListener {
            onLogout()
            dismiss()
        }
    }
}