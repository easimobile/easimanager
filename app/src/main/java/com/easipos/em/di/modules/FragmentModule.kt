package com.easipos.em.di.modules

import org.kodein.di.Kodein

fun provideFragmentModule() = Kodein.Module("fragmentModule") {
}
