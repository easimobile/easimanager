package com.easipos.em.di.modules

import android.app.Application
import androidx.room.Room
import com.easipos.em.Easi
import com.easipos.em.api.misc.AuthInterceptor
import com.easipos.em.api.misc.AuthOkhttpClient
import com.easipos.em.api.misc.TokenAuthenticator
import com.easipos.em.api.services.Api
import com.easipos.em.datasource.DataFactory
import com.easipos.em.executor.*
import com.easipos.em.managers.FcmManager
import com.easipos.em.managers.PushNotificationManager
import com.easipos.em.repositories.notification.NotificationDataRepository
import com.easipos.em.repositories.notification.NotificationRepository
import com.easipos.em.repositories.precheck.PrecheckDataRepository
import com.easipos.em.repositories.precheck.PrecheckRepository
import com.easipos.em.room.RoomService
import com.easipos.em.services.FcmService
import com.easipos.em.services.PushNotificationService
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import io.github.anderscheow.validator.Validator
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.eagerSingleton
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

fun provideCommonModule(easi: Easi) = Kodein.Module("commonModule") {
    bind<Easi>() with singleton { instance<Application>() as Easi }
    bind<Validator>() with singleton { Validator.with(easi) }
    bind<PushNotificationManager>() with singleton { PushNotificationManager(PushNotificationService()) }
    bind<FcmManager>() with singleton { FcmManager(FcmService(easi)) }
    bind<ThreadExecutor>() with singleton { JobExecutor() }
    bind<PostExecutionThread>() with singleton { UIThread() }
    bind<DiskIOExecutor>() with singleton { IOExecutor() }

    bind<DataFactory>() with singleton {
        DataFactory(instance(), instance(), instance(), instance(), instance())
    }

    bind<PrecheckRepository>() with singleton { PrecheckDataRepository(instance()) }
    bind<NotificationRepository>() with singleton {
        NotificationDataRepository(instance())
    }
}

fun provideApiModule(userAgent: String, endpoint: String, authorisation: String) = Kodein.Module("apiModule") {
    bind<AuthInterceptor>() with singleton { AuthInterceptor(userAgent, authorisation) }
    bind<TokenAuthenticator>() with singleton { TokenAuthenticator(instance()) }
    bind<AuthOkhttpClient>() with singleton { AuthOkhttpClient(instance(), instance()) }
    bind<RxJava2CallAdapterFactory>() with singleton { RxJava2CallAdapterFactory.create() }
    bind<GsonConverterFactory>() with singleton {
        val gson = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
            .create()
        GsonConverterFactory.create(gson)
    }
    bind<Api>() with singleton {
        Retrofit.Builder()
            .baseUrl(endpoint)
            .client(instance<AuthOkhttpClient>().getAuthOkhttpClient())
            .addCallAdapterFactory(instance())
            .addConverterFactory(instance())
            .build()
            .create(Api::class.java)
    }
}

fun provideDatabaseModule(easi: Easi, dbName: String) = Kodein.Module("databaseModule") {
    bind<RoomService>() with eagerSingleton {
        Room.databaseBuilder(easi.applicationContext,
            RoomService::class.java, dbName)
            .fallbackToDestructiveMigration()
            .build()
    }
}