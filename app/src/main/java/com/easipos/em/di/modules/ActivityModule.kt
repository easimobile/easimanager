package com.easipos.em.di.modules

import com.easipos.em.activities.login.navigation.LoginNavigation
import com.easipos.em.activities.login.navigation.LoginNavigationImpl
import com.easipos.em.activities.main.navigation.MainNavigation
import com.easipos.em.activities.main.navigation.MainNavigationImpl
import com.easipos.em.activities.reset_password.navigation.ResetPasswordNavigation
import com.easipos.em.activities.reset_password.navigation.ResetPasswordNavigationImpl
import com.easipos.em.activities.splash.navigation.SplashNavigation
import com.easipos.em.activities.splash.navigation.SplashNavigationImpl
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

fun provideActivityModule() = Kodein.Module("activityModule") {
    bind<SplashNavigation>() with provider { SplashNavigationImpl() }
    bind<LoginNavigation>() with provider { LoginNavigationImpl() }
    bind<ResetPasswordNavigation>() with provider { ResetPasswordNavigationImpl() }
    bind<MainNavigation>() with provider { MainNavigationImpl() }
}
