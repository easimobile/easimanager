package com.easipos.em.api.requests

import com.easipos.em.managers.UserManager
import okhttp3.MultipartBody

open class RequestModel(var apiKey: String? = UserManager.token?.token) {

    open fun toFormDataBuilder(): MultipartBody.Builder {
        val builder = MultipartBody.Builder()
            .setType(MultipartBody.FORM)

        if (apiKey != null) {
            builder.addFormDataPart("apiKey", apiKey!!)
        }

        return builder
    }
}