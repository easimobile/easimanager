package com.easipos.em.use_cases.notification

import com.easipos.em.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.em.repositories.notification.NotificationRepository
import com.easipos.em.use_cases.base.AbsRxCompletableUseCase
import io.reactivex.Completable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class RegisterFcmTokenUseCase(kodein: Kodein)
    : AbsRxCompletableUseCase<RegisterFcmTokenUseCase.Params>(kodein) {

    private val repository: NotificationRepository by kodein.instance()

    override fun createCompletable(params: Params): Completable =
        repository.registerFcmToken(params.model)

    class Params private constructor(val model: RegisterFcmTokenRequestModel) {
        companion object {
            fun createQuery(model: RegisterFcmTokenRequestModel): Params {
                return Params(model)
            }
        }
    }
}
