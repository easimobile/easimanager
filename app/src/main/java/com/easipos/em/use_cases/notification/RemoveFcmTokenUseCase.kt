package com.easipos.em.use_cases.notification

import com.easipos.em.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.em.repositories.notification.NotificationRepository
import com.easipos.em.use_cases.base.AbsRxCompletableUseCase
import io.reactivex.Completable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class RemoveFcmTokenUseCase(kodein: Kodein)
    : AbsRxCompletableUseCase<RemoveFcmTokenUseCase.Params>(kodein) {

    private val repository: NotificationRepository by kodein.instance()

    override fun createCompletable(params: Params): Completable =
        repository.removeFcmToken(params.model)

    class Params private constructor(val model: RemoveFcmTokenRequestModel) {
        companion object {
            fun createQuery(model: RemoveFcmTokenRequestModel): Params {
                return Params(model)
            }
        }
    }
}
