package com.easipos.em.datasource.precheck

import com.easipos.em.api.misc.SingleObserverRetrofit
import com.easipos.em.api.requests.precheck.CheckVersionRequestModel
import com.easipos.em.api.services.Api
import com.easipos.em.executor.PostExecutionThread
import com.easipos.em.executor.ThreadExecutor
import com.easipos.em.use_cases.error
import com.easipos.em.use_cases.success
import com.orhanobut.logger.Logger
import io.reactivex.Single

class PrecheckDataSource(private val api: Api,
                         private val threadExecutor: ThreadExecutor,
                         private val postExecutionThread: PostExecutionThread) : PrecheckDataStore {

    override fun checkVersion(model: CheckVersionRequestModel): Single<Boolean> =
        Single.create { emitter ->
            api.checkVersion(model.toFormDataBuilder().build())
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<Boolean>() {
                    override fun onResponseSuccess(responseData: Boolean) {
                        Logger.i("Version checked: $responseData")

                        emitter.success(responseData)
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }
}
