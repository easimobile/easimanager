package com.easipos.em.datasource.precheck

import com.easipos.em.api.requests.precheck.CheckVersionRequestModel
import io.reactivex.Single

interface PrecheckDataStore {

    fun checkVersion(model: CheckVersionRequestModel): Single<Boolean>
}
