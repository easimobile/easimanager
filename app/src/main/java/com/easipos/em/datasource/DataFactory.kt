package com.easipos.em.datasource

import android.app.Application
import com.easipos.em.api.services.Api
import com.easipos.em.datasource.notification.NotificationDataSource
import com.easipos.em.datasource.notification.NotificationDataStore
import com.easipos.em.datasource.precheck.PrecheckDataSource
import com.easipos.em.datasource.precheck.PrecheckDataStore
import com.easipos.em.executor.PostExecutionThread
import com.easipos.em.executor.ThreadExecutor
import com.easipos.em.room.RoomService

class DataFactory(private val application: Application,
                  private val api: Api,
                  private val roomService: RoomService,
                  private val threadExecutor: ThreadExecutor,
                  private val postExecutionThread: PostExecutionThread) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(api, threadExecutor, postExecutionThread)

    fun createNotificationDataSource(): NotificationDataStore =
        NotificationDataSource(api, threadExecutor, postExecutionThread)
}
