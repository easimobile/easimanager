package com.easipos.em.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.easipos.em.models.Notification

@Database(
        entities = [
            Notification::class
        ],
        version = 1,
        exportSchema = false
)
@TypeConverters(Converters::class)
abstract class RoomService : RoomDatabase() {

    abstract fun notificationDao(): NotificationDao
}
