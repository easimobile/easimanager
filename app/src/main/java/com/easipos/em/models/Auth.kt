package com.easipos.em.models

data class Auth(val token: String)