package com.easipos.em.activities.reset_password.mvp

import android.app.Application
import com.easipos.em.base.Presenter

class ResetPasswordPresenter(application: Application)
    : Presenter<ResetPasswordView>(application)
