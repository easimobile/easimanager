package com.easipos.em.activities.splash.mvp

import android.app.Application
import com.easipos.em.api.requests.precheck.CheckVersionRequestModel
import com.easipos.em.base.Presenter
import com.easipos.em.managers.UserManager
import com.easipos.em.tools.Preference
import com.easipos.em.use_cases.base.DefaultSingleObserver
import com.easipos.em.use_cases.precheck.CheckVersionUseCase

class SplashPresenter(application: Application)
    : Presenter<SplashView>(application) {

    private val checkVersionUseCase by lazy { CheckVersionUseCase(kodein) }

    fun checkVersion() {
        checkVersionUseCase.execute(object : DefaultSingleObserver<Boolean>() {
            override fun onSuccess(value: Boolean) {
                super.onSuccess(value)
                if (value) {
                    view?.showUpdateAppDialog()
                } else {
                    checkIsAuthenticated()
                }
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                checkIsAuthenticated()
            }
        }, CheckVersionUseCase.Params.createQuery(CheckVersionRequestModel()))
    }

    fun checkIsAuthenticated() {
        if (Preference.prefIsLoggedIn && UserManager.token != null) {
            view?.navigateToMain()
        } else {
            view?.navigateToLogin()
        }
    }
}
