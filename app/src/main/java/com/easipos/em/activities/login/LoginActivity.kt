package com.easipos.em.activities.login

import android.content.Context
import android.content.Intent
import com.easipos.em.R
import com.easipos.em.activities.login.mvp.LoginPresenter
import com.easipos.em.activities.login.mvp.LoginView
import com.easipos.em.activities.login.navigation.LoginNavigation
import com.easipos.em.base.CustomBaseAppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.generic.instance

class LoginActivity : CustomBaseAppCompatActivity(), LoginView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    //region Variables
    private val navigation: LoginNavigation by instance()

    private val presenter by lazy { LoginPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_login

    override fun init() {
        super.init()
        presenter.onAttachView(this)

        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }
    //endregion

    //region Action Methods
    private fun setupListeners() {
        text_view_forgot_password.setOnClickListener {
            navigation.navigateToResetPassword(this)
        }
    }
    //endregion
}
