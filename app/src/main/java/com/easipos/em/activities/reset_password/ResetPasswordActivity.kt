package com.easipos.em.activities.reset_password

import android.content.Context
import android.content.Intent
import com.easipos.em.R
import com.easipos.em.activities.reset_password.mvp.ResetPasswordPresenter
import com.easipos.em.activities.reset_password.mvp.ResetPasswordView
import com.easipos.em.activities.reset_password.navigation.ResetPasswordNavigation
import com.easipos.em.base.CustomBaseAppCompatActivity
import kotlinx.android.synthetic.main.activity_reset_password.*
import org.kodein.di.generic.instance

class ResetPasswordActivity : CustomBaseAppCompatActivity(), ResetPasswordView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, ResetPasswordActivity::class.java)
        }
    }

    //region Variables
    private val navigation: ResetPasswordNavigation by instance()

    private val presenter by lazy { ResetPasswordPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_reset_password

    override fun init() {
        super.init()
        presenter.onAttachView(this)

        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }
    //endregion

    //region Action Methods
    private fun setupListeners() {
        image_view_back.setOnClickListener {
            finish()
        }
    }
    //endregion
}
