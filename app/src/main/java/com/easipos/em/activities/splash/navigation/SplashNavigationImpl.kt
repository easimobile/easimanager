package com.easipos.em.activities.splash.navigation

import android.app.Activity
import com.easipos.em.activities.login.LoginActivity
import com.easipos.em.activities.main.MainActivity

class SplashNavigationImpl : SplashNavigation {

    override fun navigateToLogin(activity: Activity) {
        activity.startActivity(LoginActivity.newIntent(activity))
        activity.finishAffinity()
    }

    override fun navigateToMain(activity: Activity) {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finishAffinity()
    }
}