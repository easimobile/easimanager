package com.easipos.em.activities.login.mvp

import android.app.Application
import com.easipos.em.base.Presenter

class LoginPresenter(application: Application)
    : Presenter<LoginView>(application)
