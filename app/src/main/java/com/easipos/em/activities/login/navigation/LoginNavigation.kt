package com.easipos.em.activities.login.navigation

import android.app.Activity

interface LoginNavigation {

    fun navigateToResetPassword(activity: Activity)

    fun navigateToMain(activity: Activity)
}