package com.easipos.em.activities.main

import android.content.Context
import android.content.Intent
import androidx.core.view.GravityCompat
import com.easipos.em.R
import com.easipos.em.activities.main.mvp.MainPresenter
import com.easipos.em.activities.main.mvp.MainView
import com.easipos.em.activities.main.navigation.MainNavigation
import com.easipos.em.base.CustomBaseAppCompatActivity
import com.easipos.em.managers.FcmManager
import com.easipos.em.managers.PushNotificationManager
import com.onesignal.OneSignal
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.generic.instance

class MainActivity : CustomBaseAppCompatActivity(), MainView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    //region Variables
    private val navigation: MainNavigation by instance()
    private val fcmManager: FcmManager by instance()
    private val pushNotificationManager: PushNotificationManager by instance()

    private val presenter by lazy { MainPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onBackPressed() {
        when {
            drawerLayout.isDrawerOpen(GravityCompat.START) -> drawerLayout.closeDrawer(GravityCompat.START)
            drawerLayout.isDrawerOpen(GravityCompat.END) -> drawerLayout.closeDrawer(GravityCompat.END)
            else -> super.onBackPressed()
        }
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_main

    override fun init() {
        super.init()
        presenter.onAttachView(this)

        observeOneSignalSubscription()
        registerPushTokenIfPossible()
        processPayload()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }
    //endregion

    //region Action Methods
    fun processPayload() {
        Logger.d(pushNotificationManager.payload)
        pushNotificationManager.payload?.let { _ ->
//            navigateToNotification()
            pushNotificationManager.removePayload()
        }
    }

    private fun observeOneSignalSubscription() {
        OneSignal.addSubscriptionObserver { state ->
            Logger.d(state.toString())
            fcmManager.service.saveFcmToken(state.to.userId)
        }
    }

    private fun registerPushTokenIfPossible() {
        fcmManager.service.registerFcmToken()
    }

    private fun setupListeners() {
        image_view_menu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START)
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }
    }
    //endregion
}
