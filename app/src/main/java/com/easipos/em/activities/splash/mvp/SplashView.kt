package com.easipos.em.activities.splash.mvp

import com.easipos.em.base.View

interface SplashView : View {

    fun showUpdateAppDialog()

    fun navigateToLogin()

    fun navigateToMain()
}
