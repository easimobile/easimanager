package com.easipos.em.activities.login.navigation

import android.app.Activity
import com.easipos.em.activities.main.MainActivity
import com.easipos.em.activities.reset_password.ResetPasswordActivity

class LoginNavigationImpl : LoginNavigation {

    override fun navigateToResetPassword(activity: Activity) {
        activity.startActivity(ResetPasswordActivity.newIntent(activity))
    }

    override fun navigateToMain(activity: Activity) {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finishAffinity()
    }
}