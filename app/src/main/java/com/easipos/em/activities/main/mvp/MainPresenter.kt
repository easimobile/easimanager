package com.easipos.em.activities.main.mvp

import android.app.Application
import com.easipos.em.base.Presenter

class MainPresenter(application: Application)
    : Presenter<MainView>(application)
