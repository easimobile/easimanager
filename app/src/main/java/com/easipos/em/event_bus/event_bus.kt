package com.easipos.em.event_bus

data class NotificationCount(val count: Int)